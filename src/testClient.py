#! /usr/bin/env python

from __future__ import print_function

import roslib
import rospy
from sensor_msgs.msg import Image

# Brings in the SimpleActionClient
import actionlib

# Brings in the messages used by the fibonacci action, including the
# goal message and the result message.
roslib.load_manifest('object_detection_yolo_opencv4')
import object_detection_yolo_opencv4.msg

def count_objects_client():
    # Creates the SimpleActionClient, passing the type of the action
    # (FibonacciAction) to the constructor.
    client = actionlib.SimpleActionClient('count_objects', object_detection_yolo_opencv4.msg.count_objectsAction)

    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()

    # Creates a goal to send to the action server.
    goal = object_detection_yolo_opencv4.msg.count_objectsGoal()
    goal.image_raw = rospy.wait_for_message('/xtion/rgb/image_raw', Image)
    goal.confidence = 0.3
    goal.nms = 0.3

    # Sends the goal to the action server.
    client.send_goal(goal)

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result()  # A FibonacciResult

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('count_objects_client_py')
        result = count_objects_client()
        print(result.object_count)
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)
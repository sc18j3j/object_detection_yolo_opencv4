#!/usr/bin/python

import sys
sys.path.insert(0,'/home/cv2-package')
import os

import rospy
import actionlib
import object_detection_yolo_opencv4.msg
from sensor_msgs.msg import Image

import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError

# ROS OBJECT DETECTION USING YOLO:
#
# subscribes to Tiago xtion camera
# converts xtion image to opencv image
# image convereted to blob and fed through Deep Neural Net loaded with YOLOv3 weights
# resulting layers are looped over for high confidence + image overlayed with bounding boxes
#
#
#
# REFERENCES TO CODE USED:
#
# CVBridge using ROS tutorial: http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython
# YOLO OpenCV using Pyimagesearch: https://www.pyimagesearch.com/2018/11/12/yolo-object-detection-with-opencv/
# YOLOv3: https://pjreddie.com/darknet/yolo/

class count_objects_server(object):
    _feedback = object_detection_yolo_opencv4.msg.count_objectsFeedback()
    _result = object_detection_yolo_opencv4.msg.count_objectsResult()

    def __init__(self, name):
        # setup action server
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, object_detection_yolo_opencv4.msg.count_objectsAction, execute_cb = self.execute_cb, auto_start = False)
        self._as.start()
        rospy.loginfo('count_objects action server initialised!')
    
    def execute_cb(self, goal):
        rospy.loginfo('detecting objects. dataset: %s, confidence:%f, nms:%f', goal.dataset, goal.confidence, goal.nms)

        # set up YOLO DNN
        path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/dnn_weights/" + goal.dataset + '/'
    	net = cv2.dnn.readNetFromDarknet(path + "yolov3.cfg", path + "yolov3.weights")
    	ln = net.getLayerNames()
    	ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]
        # labels and colours for object detection boxes
        labels = open(path + "classes.txt").read().strip().split("\n")
        np.random.seed(42)
    	COLORS = np.random.randint(0, 255, size=(len(labels), 3), dtype="uint8")

        # get the Image and try to convert to opencv array
        image_msg = goal.image_raw
    	try:
            bridge = CvBridge()
            frame = bridge.imgmsg_to_cv2(image_msg, "bgr8")
    	except CvBridgeError as e:
            rospy.loginfo(e)
            self._as.set_aborted(self._result)
            return
        
        # convert cv image to blob and feed through DNN
        blob = cv2.dnn.blobFromImage(frame, 1/255.0, (416,416), swapRB=True, crop=False)
        net.setInput(blob)
        layerOutputs = net.forward(ln)

        # setup required lists
        (rows, cols) = frame.shape[:2]
        boxes = []
        confidences = []
        classIDs = []

        # loop over resulting layer and choose objects with confidence > 0.5
        for output in layerOutputs:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                if confidence > goal.confidence:
                    # get the size of the box
                    box = detection[0:4] * np.array([cols, rows, cols, rows])
                    (centerX, centerY, width, height) = box.astype("int")
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))
                    # append current object to arrays
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

        # non-maximum suppression to remove overlaps
        keptIndices = cv2.dnn.NMSBoxes(boxes, confidences, goal.confidence, goal.nms)

        # setup Detection msg array
        detected_objects = []

        # ensure at least one detection exists
        if len(keptIndices) > 0:
            # loop over the indices kept from NMS
            for i in keptIndices.flatten():
                # extract name, confidence, bounding box coords
                name = labels[classIDs[i]]
                confidence = confidences[i]
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                # append object to Detection array
                detected_objects.append(object_detection_yolo_opencv4.msg.Detection( \
                    name = name, \
                    confidence = confidence, \
                    xywh = [x, y, w, h]))
                # draw a bounding box rectangle and label on the image
                color = [int(c) for c in COLORS[classIDs[i]]]
                cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                text = "{}: {:.4f}".format(name, confidence)
                cv2.putText(frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

        # create sensor_msgs Image
        image = Image()
        image.header.stamp = rospy.Time.now()
        image.header.frame_id = 'xtion_bgr_optical_frame'
        image.height = rows
        image.width = cols
        image.encoding = 'bgr8'
        image.is_bigendian = 0
        image.step = cols * 3
        image.data = list(frame.reshape(rows * cols * 3))
        # set result
        self._result.image_bb = image
        self._result.detected_objects = detected_objects
        # success
        rospy.loginfo('object detection successful')
        self._as.set_succeeded(self._result)

if __name__ == '__main__':
    # spin the node
    rospy.init_node('count_objects')
    server = count_objects_server(rospy.get_name())
    rospy.spin()
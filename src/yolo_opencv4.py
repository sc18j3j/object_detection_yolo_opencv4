#!/usr/bin/python

from __future__ import print_function

import roslib
roslib.load_manifest('object_detection_yolo_opencv4')
import sys
sys.path.insert(0,'/home/cv2-package')
import os
import argparse
import rospy
import numpy as np
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

# ROS OBJECT DETECTION USING YOLO:
#
# subscribes to Tiago xtion camera
# converts xtion image to opencv image
# image convereted to blob and fed through Deep Neural Net loaded with YOLOv3 weights
# resulting layers are looped over for high confidence + image overlayed with bounding boxes
#
#
#
# REFERENCES TO CODE USED:
#
# CVBridge using ROS tutorial: http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython
# YOLO OpenCV using Pyimagesearch: https://www.pyimagesearch.com/2018/11/12/yolo-object-detection-with-opencv/
# YOLOv3: https://pjreddie.com/darknet/yolo/


class yolo_detector:

    def __init__(self):
        # store confidence and nms arguments
        self.confidence = args["confidence"]
        self.nms = args["nms"]
        
        # labels & colours for boxes
    	np.random.seed(42)
        self.labels = open(path + "classes.txt").read().strip().split("\n")
    	self.COLORS = np.random.randint(0, 255, size=(len(self.labels), 3), dtype="uint8")

        # set up YOLO DNN
        weightsPath = path + "yolov3_final.weights"
    	configPath = path + "yolov3.cfg"
    	self.net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
    	self.ln = self.net.getLayerNames()
    	self.ln = [self.ln[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]

        # set up bridge to opencv + publisher/subscriber
        self.bridge = CvBridge()
        self.result_pub = rospy.Publisher("/yolo_opencv4/object_detected", String, queue_size=10)
        self.image_sub = rospy.Subscriber(args["topic"], Image, self.callback)


    def callback(self,data):
        # try to convert xtion image to opencv
    	try:
    	    frame = self.bridge.imgmsg_to_cv2(data, "bgr8")
    	except CvBridgeError as e:
    	     print(e)

        # convert cv image to blob and feed through DNN
        blob = cv2.dnn.blobFromImage(frame, 1/255.0, (416,416), swapRB=True, crop=False)
        self.net.setInput(blob)
        layerOutputs = self.net.forward(self.ln)

        # setup required lists
        (H, W) = frame.shape[:2]
        boxes = []
        confidences = []
        classIDs = []
        result = []

        # loop over resulting layer and choose objects with confidence > 0.5
        for output in layerOutputs:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                if confidence > self.confidence:
                    # get the size of the box
                    box = detection[0:4] * np.array([W,H,W,H])
                    (centerX, centerY, width, height) = box.astype("int")
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))
                    # append current object to arrays
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

        # non-maximum suppression to remove overlaps
        keptIndices = cv2.dnn.NMSBoxes(boxes, confidences, self.confidence, self.nms)

        # ensure at least one detection exists
        if len(keptIndices) > 0:
            # loop over the indices kept from NMS
            for i in keptIndices.flatten():
                # extract the bounding box coordinates and store results
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                resultString = [self.labels[classIDs[i]], str(confidences[i]), str(x), str(y), str(w), str(h)]
                result.append(','.join(resultString))
                # draw a bounding box rectangle and label on the image
                color = [int(c) for c in self.COLORS[classIDs[i]]]
                cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                text = "{}: {:.4f}".format(self.labels[classIDs[i]], confidences[i])
                cv2.putText(frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
        
        # publish results
        result_string = ':'.join(result)
        rospy.loginfo(result_string)
        self.result_pub.publish(result_string)

        # show the output image
        cv2.imshow("Image", frame)
        cv2.waitKey(1)

def main():
    yd = yolo_detector()
    rospy.init_node('yolo_opencv4', anonymous=False)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/dnn_weights/"
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--topic", default="/xtion/rgb/image_raw", help="ros topic to subscribe to for image feed")
    parser.add_argument("-c", "--confidence", type=float, default=0.5, help="minimum probability for object detection")
    parser.add_argument("-n", "--nms", default=0.3, type=float, help="threshold for non-maximum supression to remove overlapping detections")
    args = vars(parser.parse_args())
    main()
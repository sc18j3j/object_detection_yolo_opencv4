# Installation

**opencv4:**

Install python opencv4 using the following tutorial: https://www.pyimagesearch.com/2018/08/15/how-to-install-opencv-4-on-ubuntu/. 
apt-upgrade and virtualenv setup are not required.

Place cv2.so from opencv4 build located at `~/opencv/build/lib/cv2.so` in a location such that the Python function `sys.path.insert(0,'<cv2.so location>')` can access it.

The use of the path insert above, followed by `import cv2`, will then load opencv4 for that instance of Python only, thus allowing temporary support for the required DNN module.

**object_detection_yolo_opencv4**

Clone this repository into the workspace src folder.

    git clone https://gitlab.com/joejeffcock/object_detection_yolo_opencv4.git
    
at workspace folder:

    catkin_make
    
Default path at line 131 `default="/home/jeff/jeff_ws/src/object_detection_yolo_opencv4/dnn_weights/coco"` should be changed to the full path of coco at the installation destination,
or another path can be specified via command line argument.

# yolo_opencv4.py (not maintained)
ROS module that uses opencv4's Deep Neural Network functionality to detect objects using YOLOv3 (You Only Look Once) on an rgb image feed via rostopic.

* Subscribes to rgb/image_raw output of a camera in ROS
* Converts camera image to opencv image.
* Image converted to blob and fed through opencv4 Deep Neural Network loaded with YOLOv3 weights.
* Resulting layers are looped over to single out object detections with high confidence.
* Camera image is overlayed with bounding boxes for objects detected and displayed.
* Detected objects and their confidences/coordinates are published to ros topic `/yolo_opencv4/object_detected`.

<br/>

**Run yolo_opencv4 for ROS**

In the workspace folder:

    source devel/setup.bash

then

    rosrun object_detection_yolo_opencv4 yolo_opencv4.py
    
while roscore is running with a camera topic **OR**

    rosrun object_detection_yolo_opencv4 yolo_opencv4.py <optional arguments>
    
can be used to pass a specific path/topic/confidence/nms for detection as specified below.

<br/>

**Command Line Arguments**

| Argument | Alt | Description |
| ------ | ------ | ------ |
| --path | -p | path to folder containing names, yolov3.weights and yolov3.cfg for DNN |
| --topic | -t | ros topic to subscribe to for image feed	 |
| --confidence | -c | minimum probability for object detection |
| --nms | -n | threshold for non-maximum suppression to remove overlapping detections |
| --help | -h | view help |

When no arguments are passed, the following defaults are used:

	path:= "/home/jeff/jeff_ws/src/object_detection_yolo_opencv4/dnn_weights/coco"
	topic:= "/xtion/rgb/image_raw"
	confidence:= 0.5
	nms:= 0.3

<br/>

**Output to roscore**

Detections are published by the object detector to roscore via topic `/yolo_opencv4/object_detected`

Output is a string where objects are separated by ':'s and their properties separated by ','s:

`
String object_name,float confidence,float x,float y,float width,float height: ... : ...
`

Objects can be accessed in python by string split.

<br/>

**Current limitations**

* String output to roscore requires additional computation; msgs would be ideal instead
* opencv4 - no issues have been encountered using other Tiago modules with opencv4 installed... yet (Tiago uses opencv 3.3.1)
* gazebo - slow publish rate from xtion topic using gazebo; significant input lag from camera during simulation

# References to code used:

CVBridge by ROS.org: http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython

YOLO OpenCV by Pyimagesearch: https://www.pyimagesearch.com/2018/11/12/yolo-object-detection-with-opencv/

YOLOv3 by Redmon, Joseph and Farhadi, Ali: https://pjreddie.com/darknet/yolo/
